MYSQL=mysql:8.0.31
docker run --name mysql8 -d \
 -e MYSQL_ROOT_PASSWORD=root \
 -e MYSQL_USER=chainmaker \
 -e MYSQL_PASSWORD=chainmaker \
 -e MYSQL_DATABASE=chainmaker_recorder \
 -v mysql8-data:/var/lib/mysql \
 -p 3306:3306 \
 ${MYSQL}