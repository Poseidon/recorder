# ip 需要填写为 chainmaker-go 所在的机器IP
serverAddress=127.0.0.1:9527

# 查看 recorder.RegisterModel() 注册了哪些 model
registerinfo:
	@curl ${serverAddress}/v1/config/registerinfo

# 查看 recorder.Record() 允许记录哪些 model
accessconfig:
	@curl ${serverAddress}/v1/config/accessconfig

# 更新 recorder.Record() 允许记录的 model
updateaccessconfig:
	@curl -XPUT \
	--data-binary @accessconfig.yml \
	${serverAddress}/v1/config/accessconfig

# 查看 recorder.GetConfigValue(key) 可以获取哪些数据
configvalue:
	@curl ${serverAddress}/v1/config/configvalue

# 更新 recorder.GetConfigValue(key) 可以获取到的数据
updateconfigvalue:
	@curl -XPUT \
	--data-binary @configvalue.yml \
	${serverAddress}/v1/config/configvalue
