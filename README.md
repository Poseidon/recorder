# Recorder

用于记录数据到数据库

# 用法

## 数据定义

在项目中引入 recorder 包，并定义需要记录到数据库中的数据结构原型，然后在 `init()` 函数中使用 `recorder.RegisterModel()` 函数注册数据模型：

```go
package some_package

import (
  "chainmaker.org/chainmaker/recorder"
  ......
)

type SampleTransaction struct { // 数据库中对应的 tableName 为 sample_transaction
  ID        uint      `gorm:"primarykey"` // 所有记录的数据，如无特殊需求，建议都使用 ID 作为 primarykey
  TxID      string    `gorm:"type:char(64)"` // fieldName 建议都使用 CamelCase 的写法
  CreatedAt time.Time `gorm:"index"`
  Source    int
}
type SampleStruct struct {
  ID        uint      `gorm:"primarykey"`
  TxID      string    `gorm:"type:char(64)"`
  CreatedAt time.Time `gorm:"index"` // 如果 CreatedAt 字段不填充值，gorm 框架会自动填充 CreatedAt 字段的值为当前时间
  Source    int
}

func init() {
  // 支持可变数量参数，但是每一个参数都必须是Object指针
  recorder.RegisterModel(&SampleTransaction{}, &SampleStruct{})
}
```

## 记录数据

在需要记录数据的地方，构造数据 Object，然后使用 `recorder.Record()` 函数将数据记录到数据库

```go
package some_package

import (
  "chainmaker.org/chainmaker/recorder"
  ......
)

func (pool *normalPool) AddTx(tx *commonPb.SampleTransaction, source protocol.TxSource) error {
      ......
      val := &SampleTransaction{ // 必须使用指针
        //ID : ID 字段不用填写，数据库会自动生成正确的值
        TxID:      tx.Payload.GetTxId(),
        // CreatedAt: time.Now(), // CreatedAt 可以不填写，gorm 会自动为该字段填充当前时间
        Source:    int(source),
      }
      recorder.Record(val, nil) // 异步执行，记录到数据库
      ......
      return nil
}
```

## 等待记录结果

`recorder.Record()` 函数是异步执行的，如果 `caller` 关注是否记录数据成功，可以向函数第二个参数传入一个 `chan error`，用于接收记录结果

```go
package some_package

import (
  "chainmaker.org/chainmaker/recorder"
  ......
)

func (pool *normalPool) AddTx(tx *commonPb.SampleTransaction, source protocol.TxSource) error {
      ......
      val := &SampleTransaction{ // 必须使用指针
        //ID : ID 字段不用填写，数据库会自动生成正确的值
        TxID:      tx.Payload.GetTxId(),
        // CreatedAt: time.Now(), // CreatedAt 可以不填写，gorm 会自动为该字段填充当前时间
        Source:    int(source),
      }
      resultC := make(chan error, 1)
      recorder.Record(val, resultC) // 异步执行，记录到数据库

      select {
        case err := <- resultC:
          if err != nil {
            // 记录数据出现错误
          } else {
            // 记录数据成功
          }
        case <- time.NewTimer(time.Second).C:
          // 记录数据超时
      }
      ......
      return nil
}
```

## 启动数据库

如果要在本机进行测试，可以使用 `docker` 在本地启动一个 `MySQL` 数据库：

```bash
MYSQL=mysql:8.0.31 #可以更换为其它版本

docker run --name mysql8 -d \
-e MYSQL_ROOT_PASSWORD=root \
-e MYSQL_USER=chainmaker \
-e MYSQL_PASSWORD=chainmaker \
-e MYSQL_DATABASE=chainmaker_recorder \
-v mysql8-data:/var/lib/mysql \
-p 3306:3306 \
${MYSQL}
```

连接数据库的 DNS 格式为 `"user:passwd@tcp(ip:port)/database_name?charset=utf8mb4&parseTime=True&loc=Local"`  
 使用如上方式启动的 `MySQL`，可以使用如下 `DNS` 进行连接：

```bash
"chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_recorder?charset=utf8mb4&parseTime=True&loc=Local"
```

## 连接数据库

在主程序启动阶段，使用 `recorder.Start(dns, port)` 函数连接数据库  
参数解释：

- `dns`  
  数据库的 dns；如果 dns 字符串包含 `"sqlite"`，如 `"some_path/sqlite.db"`，那么 `recorder` 框架会将 `dns` 当做一个文件路径，并在运行的机器上创建本地文件`"some_path/sqlite.db"`，之后 `recorder.Record()` 通过 `sqlite` 数据库引擎将数据记录到该文件。
- `port`  
  `recorder` 框架启动后，会在后台启动一个 `http server`, 并倾听该 `port` 端口，用于动态更新 `configuration`，下文会详细介绍。如果该 `port` 值为 `0`，那么 `http server` 会默认倾听 `9527` 端口。

```go
package main

import (
  "chainmaker.org/chainmaker/recorder"
  ......
)

func main() {
  ......
  recorder.Start(dns, port)
  ......
}
```

上述介绍的 `recorder` 函数，需要确保其执行顺序为：

```go
recorder.RegisterModel() // 放在 init() 函数中执行，确保了在 main() 函数之前执行

recorder.Start() // 在程序启动过程中执行

recorder.Record() // 在程序启动后的业务函数中执行
```

# 动态更新 configuration

项目启动后，`recorder` 框架会启动一个基于 `HTTP` 协议的 `configServer`， 该 `configServer` 倾听的端口即为 `recorder.Start(dns, port)` 传入的 `port` 参数，如果参数 `port` 为 `0`, `configServer` 会默认倾听 `9527` 端口。  
参考本框架内的 `Makefile` 文件，可以方便地使用 `make someCommand` 命令向 `configServer` 发送 `request`。

## 查看 `recorder.RegisterModel()` 的调用记录

使用 `make registerinfo` 命令查看注册了哪些数据类型用于记录到数据库，并可以查看调用`recorder.RegisterModel()`的代码位置

```bash
$ make registerinfo
SampleStruct: /mycode/chainmaker/recorder/recorder_test.go:37:chainmaker.org/chainmaker/recorder.TestMain
SampleTransaction: /mycode/chainmaker/recorder/recorder_test.go:37:chainmaker.org/chainmaker/recorder.TestMain
```

## 查看 `recorder.Record()` 允许记录哪些 model

```bash
$ make accessconfig
{}
```

`recorder.Record()` 会根据 `accessConfig` 中配置的数据来确定是否将数据记录到数据库

```go
package recorder

func Record(value interface{}, resultC chan error) {
	if dbConnected {
		modelName := reflect.TypeOf(value).Elem().Name()
		if hasAccess(modelName) {
			// 允许记录到数据库
		} else if resultC != nil {
			// 不允许记录到数据库
		}
	} else if resultC != nil {
		// 数据库未连接
	}
}

func hasAccess(modelName string) bool {
	accessLock.RLock()
	defer accessLock.RUnlock()

	allAccess := accessConfig[ALL]
	modelAccess := accessConfig[modelName]
	return allAccess && modelAccess
}
```

初始情况下，`recorder.Record()` 不会将任何数据记录到数据库，需要通过 `make updateaccessconfig` 命令上传允许 `recorder.Record()` 记录的数据名称

## 更新 `recorder.Record()` 允许记录的 model

```bash
$ make updateaccessconfig
ALL: true
SampleStruct: true
SampleTransaction: false

# 此时再查看 accessconfig，返回的结果就不是 {} 了
$ make accessconfig
ALL: true
SampleStruct: true
SampleTransaction: false
```

`make updateaccessconfig` 命令读取的是 `Makefile` 同目录下的 `accessconfig.yml` 文件并传送给 `configServer` 的，该文件格式如下:

```bash
$ cat accessconfig.yml
ALL: true
SampleTransaction: false
SampleStruct: true
```

以 `SampleTransaction` 为例，只有在 `ALL` 为 `true` 且 `SampleTransaction` 为 `true` 的情况下，才允许 `recorder.Record()` 记录 `SampleTransaction`。  
如果 `ALL` 为 `false`，则不允许记录任何数据。

# `recorder.GetConfigValue(key)` 的使用

在业务逻辑中，可能需要读取一些动态配置的参数，可以使用 `recorder.GetConfigValue(key)` 读取配置数据：

```go
package some_package

import (
  "chainmaker.org/chainmaker/recorder"
  ......
)

func (cal *Calculator) func DoScale(total int) int {
	scale := 1.0
	if scaleVal, ok := recorder.GetConfigValue("scale"); ok {
		if val, ok := scaleVal.(float64); ok {
			if val != 0 { // 如果是0，可以当做没有该项配置
				scale = val
			}
		}
	}
	return int(float64(total) * scale)
}
```

## 查看 `recorder.GetConfigValue(key)` 可以获取到的值

```bash
$ make configvalue
{}
```

初始情况下，`configValue` 为空，需要通过 `make updateconfigvalue` 命令上传允许 `recorder.GetConfigValue(key)` 获取的值

## 更新 `recorder.GetConfigValue(key)` 可以获取到的值

```bash
$ make updateconfigvalue
rate: 100
scale: 12.2
```

此时读取 `configVallue` 的值，就不为空了:

```bash
$ make configvalue
rate: 100
scale: 12.2
```

`make updateconfigvalue` 命令会读取 `Makefile` 同目录下的 `configvalue.yml` 文件，并上传到 `configServer`。该文件格式如下：

```bash
$ cat configvalue.yml
rate: 100
scale: 12.2
```

# 在 chainmaker-go 项目中的开发示例

## 添加 `recorder.Start(dns, port)` 参数的配置项

- 在 `chainmaker-go/config/config_tpl/chainmaker.tpl` 文件的末尾，添加如下配置内容。这些配置内容默认是被注释掉的。
  ```yml
  #plugin:
  #  # Configs for recorder measure
  #  recorder:
  #    # mysql address, format: 'user:passwd@tcp(ip:port)/database_name?someconfig'
  #    dns: chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_recorder?charset=utf8mb4&parseTime=True&loc=Local
  #    # the port of recorder config server, we use it to dynamically update recorder config
  #    config_server_port: 9527
  ```
  参考 chainmaker 官方文档 [通过命令行体验链](https://docs.chainmaker.org.cn/v2.3.0/html/quickstart/%E9%80%9A%E8%BF%87%E5%91%BD%E4%BB%A4%E8%A1%8C%E4%BD%93%E9%AA%8C%E9%93%BE.html) 搭建集群的时候，在生成的 release 目录下的各个节点中，应当将`chainmaker.yml`文件末尾的这些注释解除，并将 dns 修改成自己提供的 msyql 的 dns; 如果基于同一份代码生成多个 node，且各个 node 在同一台机器上部署运行，那么需要修改各个 node 的`config_server_port`为不同的值，避免端口冲突。如下所示：
  ```yaml
  plugin:
    # Configs for recorder measure
    recorder:
      # mysql address, format: 'user:passwd@tcp(ip:port)/database_name?someconfig'
      dns: chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_recorder?charset=utf8mb4&parseTime=True&loc=Local
      # the port of recorder config server, we use it to dynamically update recorder config
      config_server_port: 9527
  ```
- 对应于上述的 `plugin` 配置项，在 `git.chainmaker.org.cn/chainmaker/localconf` 仓库的 `types.go` 文件中，新添加如下 `Plugin` 结构:

  ```go
  type CMConfig struct {
      ......
      Plugin          Plugin          `mapstructure:"plugin"`
  }
  type Plugin struct {
      Recorder Recorder `mapstructure:"recorder"`
  }

  type Recorder struct {
      DNS            string `mapstructure:"dns"`
      ConfigServerPort uint16 `mapstructure:"config_server_port"`
  }
  ```

  `chainmaker-go` 引用了 `localconf` 包，并在 `chainmaker-go/main/cmd/cli_start.go` 代码中的 `initLocalConfig(cmd)` 函数中初始化了 `localconf.ChainMakerConfig` 这个包级别的导出变量。如果在 `chainmaker.yml` 中正确配置了 `plugin` 项目，则该配置项会被解析成 `localconf.ChainMakerConfig.Plugin` 这个字段。

  ```go
  func StartCMD() *cobra.Command {
      startCmd := &cobra.Command{
          ......
          RunE: func(cmd *cobra.Command, _ []string) error {
              initLocalConfig(cmd)
              ......
              return nil
          },
      }
      ......
      return startCmd
  }
  ```

## 初始化 `recorder` 包

- 在 `chainmaker-go/main/cmd/cli_start.go:mainStart()` 函数中，添加如下代码，对 `recorder` 包进行初始化：

  ```go
  package cmd

  ......

  func mainStart() {
      if localconf.ChainMakerConfig.DebugConfig.IsTraceMemoryUsage {
          traceMemoryUsage()
      }

      if len(localconf.ChainMakerConfig.Plugin.Recorder.DNS) > 0 { // 这里初始化 recorder 包; 如果注释掉 chainmaker.yml 中的 plugin 配置项，则不会对 recorder 包进行初始化。
          dns := localconf.ChainMakerConfig.Plugin.Recorder.DNS
          port := localconf.ChainMakerConfig.Plugin.Recorder.ConfigServerPort
          err := recorder.Start(dns, port)
          if err != nil {
              log.Errorf("recorder.Start(%s, %d) err: %s", dns, port, err.Error())
          } else {
              log.Infof("recorder package connected to mysql")
          }
      }

      // init chainmaker server
      chainMakerServer := blockchain.NewChainMakerServer()
      ......
  }
  ```

  至此，`recorder` 就成功 connect 到了 `MySQL`。后续可以使用 `recorder.Record()` 函数将数据记录到数据库。如果 `recorder` 没有成功 connect 到 `MySQL`，且 `recorder.Record()` 的第二个参数 `resultC` 不为 `nil`，则 `recorder.Record()` 会向 `resultC` 传入一个 `error`，提示 `"[recorder] database not connected"`。
